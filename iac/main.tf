# Connect AWS Provider
provider "aws" {
    profile = "devops"
    region = "ap-south-1"
} 

# Terraform Version
terraform {
    required_version = ">=v1.1.0"
}

# AWS Resources - VPC 
resource "aws_vpc" "cloudbinary_vpc" {
    cidr_block = "192.168.0.0/16"
    instance_tenancy = "default"
    enable_dns_support = "true"
    enable_dns_hostnames = "true"

    tags = {
        Name = "cloudbinary_vpc"
        CreatedBy = "IAC - Terraform"
    }
}
# Create the Route Table 
resource "aws_route_table" "cloudbinary_rtb_public" {
    vpc_id = aws_vpc.cloudbinary_vpc.id

    tags = {
        Name = "cloudbinary_rtb_public"
        Created_By = "IAC - Terraform"
    }
}
# Create the Route Table 
resource "aws_route_table" "cloudbinary_rtb_private" {
    vpc_id = aws_vpc.cloudbinary_vpc.id

    tags = {
        Name = "cloudbinary_rtb_private"
        Created_By = "IAC - Terraform"
    }
}
# PublicSubnet-1
resource "aws_subnet" "cloudbinary_subnet_public1" {
    vpc_id = aws_vpc.cloudbinary_vpc.id
    cidr_block = "192.168.1.0/24"
    map_public_ip_on_launch = true 
    availability_zone = "ap-south-1a"

    tags = {
        Name = "cloudbinary_subnet_public1"
        created_by = "IAC - Terraform"
    }
}

# PublicSubnet-2
resource "aws_subnet" "cloudbinary_subnet_public2" {
    vpc_id = aws_vpc.cloudbinary_vpc.id
    cidr_block = "192.168.2.0/24"
    map_public_ip_on_launch = true 
    availability_zone = "ap-south-1b"

    tags = {
        Name = "cloudbinary_subnet_public2"
        created_by = "IAC - Terraform"
    }
}

# PrivateSubnet-1
resource "aws_subnet" "cloudbinary_subnet_private1" {
    vpc_id = aws_vpc.cloudbinary_vpc.id
    cidr_block = "192.168.3.0/24"
    availability_zone = "ap-south-1a"

    tags = {
        Name = "cloudbinary_subnet_private1"
        created_by = "IAC - Terraform"
    }
}

# PrivateSubnet-1
resource "aws_subnet" "cloudbinary_subnet_private2" {
    vpc_id = aws_vpc.cloudbinary_vpc.id
    cidr_block = "192.168.4.0/24"
    availability_zone = "ap-south-1b"

    tags = {
        Name = "cloudbinary_subnet_private2"
        created_by = "IAC - Terraform"
    }
}
# Add Public Subnet-1 unto Public RouteTable
resource "aws_route_table_association" "cb_pubsubnet1" {
    subnet_id = aws_subnet.cloudbinary_subnet_public1.id
    route_table_id = aws_route_table.cloudbinary_rtb_public.id
}

# Add Public Subnet-2 unto Public RouteTable
resource "aws_route_table_association" "cb_pubsubnet2" {
    subnet_id = aws_subnet.cloudbinary_subnet_public2.id
    route_table_id = aws_route_table.cloudbinary_rtb_public.id
}
# Add Private Subnet-1 unto Private RouteTable
resource "aws_route_table_association" "cb_prisubnet1" {
    subnet_id = aws_subnet.cloudbinary_subnet_private1.id
    route_table_id = aws_route_table.cloudbinary_rtb_private.id
}

# Add Private Subnet-2 unto Private RouteTable
resource "aws_route_table_association" "cb_prisubnet2" {
    subnet_id = aws_subnet.cloudbinary_subnet_private2.id
    route_table_id = aws_route_table.cloudbinary_rtb_private.id
}

# Internet Gateway
resource "aws_internet_gateway" "cloudbinary_igw" {
    vpc_id = aws_vpc.cloudbinary_vpc.id  

    tags = {
      "Name" = "cloudbinary_igw"
      "CreatedBy" = "IAC - Terraform"
    }
  
}

# Allow Route From IGW unto PublicRTB
resource "aws_route" "cloudbinary_rtb_igw" {
    route_table_id = aws_route_table.cloudbinary_rtb_public.id  
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.cloudbinary_igw.id 
}

# Enable EIP for NAT Gatway
resource "aws_eip" "cloudbinary_eip" {
    vpc = true 
}

# Create NAT Gateway
resource "aws_nat_gateway" "cloudbinary_natgateway" {
    subnet_id = aws_subnet.cloudbinary_subnet_public1.id  
    allocation_id = aws_eip.cloudbinary_eip.allocation_id
}

# Allow Route From NAT Gateway unto PrivateRTB
resource "aws_route" "cloudbinary_rtb_natgateway" {
    route_table_id = aws_route_table.cloudbinary_rtb_private.id  
    destination_cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.cloudbinary_natgateway.id 
}

# Create SG for Bastion SSH 22 and RDP 3389 
resource "aws_security_group" "cloudbinary_sg_bastion" {
    vpc_id = aws_vpc.cloudbinary_vpc.id  
    name = "sg_bastion"
    description = "Bastion - SSH or RDP"

    # Ingress / Inbound Traffic
    ingress {
      description = "SSH"
      cidr_blocks = ["0.0.0.0/0"]
      from_port = 22
      to_port = 22
      protocol = "tcp"

    }

    # Ingress / Inbound Traffic
    ingress {
      description = "RDP"
      cidr_blocks = ["0.0.0.0/0"]
      from_port = 3389
      to_port = 3389
      protocol = "tcp"

    }

    # egress / Outbound Traffic
    egress {
        description = "All"
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "SG_BASTION"
        CreatedBy = "IAC"
    }

}


# Create SG for DMZ 80 443 22 3389 3306 
resource "aws_security_group" "cloudbinary_sg_dmz" {
    vpc_id = aws_vpc.cloudbinary_vpc.id  
    name = "sg_dmz"
    description = "DMZ - SSH - RDP - HTTP - HTTPS - DB"

    # Ingress / Inbound Traffic
    ingress {
      description = "SSH"
      cidr_blocks = ["0.0.0.0/0"]
      from_port = 22
      to_port = 22
      protocol = "tcp"

    }
    # Ingress / Inbound Traffic
    ingress {
      description = "RDP"
      cidr_blocks = ["0.0.0.0/0"]
      from_port = 3389
      to_port = 3389
      protocol = "tcp"

    }
    # Ingress / Inbound Traffic
    ingress {
      description = "HTTP"
      cidr_blocks = ["0.0.0.0/0"]
      from_port = 80
      to_port = 80
      protocol = "tcp"

    }
    # Ingress / Inbound Traffic
    ingress {
      description = "HTTPS"
      cidr_blocks = ["0.0.0.0/0"]
      from_port = 443
      to_port = 443
      protocol = "tcp"

    }
    # Ingress / Inbound Traffic
    ingress {
      description = "DB"
      cidr_blocks = ["0.0.0.0/0"]
      from_port = 3306
      to_port = 3306
      protocol = "tcp"

    }
    # egress / Outbound Traffic
    egress {
        description = "All"
        from_port = 0
        to_port = 0
        protocol = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    tags = {
        Name = "SG_DMZ"
        CreatedBy = "IAC"
    }

}

# Create a Keypair
# resource "aws_key_pair" "iac_keys" {
#     key_name = "iac_keys"
#     public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCeroW0R3EFdeAEpt5NxIqsFmlK928O/E0EEln1xT/eNuFkMWREh0HalYVMBdy6YcHp9h58DRwzaUIgss/KK5VxMl/1VFqvf7yM5hplqHG9tdb3zmjjJRyH6qlTRSUslw3rb891elFP06sPKP1UpSR6vDT4PmTyN7aBcVtO84IBaPnTP67v+3KNMYrE/PgWc6uqEuMM0WGTs/1ma8p6vVEOUQlLtGiYjKpHRAOnXHyNt8dbloVnvlF9eD3COa3trSFYcw+vZnc2J1jV7uarA9UDsAep1qc0F0nKOXkXpu9gswK1djzEaJMETmzu/Zz902jj8VDZNqYmwMzJ695SPJ2OWP0UD4/NnkRjVuSjVgrhIlEmzWRTezRK+SYqrJFmthhvPbIG8EAbxHRJ8Ez8Dw1qymm4pbRmj1fwHBFJ3dXSJ+lkUTEmHodDUczQdmYeffqfrBIILygbYKMNWEsH9X0bqpiD2rpxVmqIgFuPrrYHR1eHxXVtYmYzlytZbBpwro8= cloudbinary@Clouds-MacBook-Pro.local"
# }

# Bastion
resource "aws_instance" "cloudbinary_bastion" {
    ami = "ami-0851b76e8b1bce90b"
    instance_type = "t2.micro"
    key_name = "aws-kesav"
    subnet_id = aws_subnet.cloudbinary_subnet_public1.id  
    vpc_security_group_ids = [aws_security_group.cloudbinary_sg_bastion.id]

    tags = {
        Name = "BASTION"
        CreatedBy = "IAC"
    }
}

# WebServer
resource "aws_instance" "cloudbinary_web" {
    ami = "ami-0851b76e8b1bce90b"
    instance_type = "t2.micro"
    key_name = "aws-kesav"
    subnet_id = aws_subnet.cloudbinary_subnet_private1.id    
    vpc_security_group_ids = [aws_security_group.cloudbinary_sg_dmz.id]

    tags = {
        Name = "WEBServer"
        CreatedBy = "IAC"
    }
}